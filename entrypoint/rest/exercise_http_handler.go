package rest

import (
	"encoding/json"
	"net/http"
	"physical-health/domain"
	"time"

	"github.com/julienschmidt/httprouter"
)

//go:generate mockgen -destination=mocks/exercise_repository_mock.go -package=mocks . ExerciseRepository
type ExerciseRepository interface {
	Save(domain.Exercise) error
	FindById(string) (*domain.Exercise, error)
}

//go:generate mockgen -destination=mocks/movement_repository_mock.go -package=mocks . MovementRepository
type MovementRepository interface {
	FindByPatient(string) ([]domain.Movement, error)
}

type (
	movement struct {
		ID         int              `json:"id"`
		PatientID  string           `json:"patientId"`
		ExerciseID string           `json:"exerciseId"`
		Repetition int              `json:"repetition"`
		Motion     []float64        `json:"motion"`
		Status     string           `json:"status"`
		StartedAt  time.Time        `json:"startedAt"`
		Results    []movementResult `json:"results"`
	}

	movementResult struct {
		Name   string  `json:"name"`
		Status string  `json:"status"`
		Value  float64 `json:"value"`
	}

	exercise struct {
		ID              string              `json:"id"`
		Name            string              `json:"name"`
		Repetitions     int                 `json:"repetitions"`
		ExpectedMotions []float64           `json:"expectedMotions"`
		Thresholds      []exerciseThreshold `json:"thresholds"`
	}

	exerciseThreshold struct {
		Name  string  `json:"name"`
		Value float64 `json:"value"`
	}

	ExerciseHandler struct {
		movementRepository MovementRepository
		exerciseRepository ExerciseRepository
	}
)

func NewExerciseHandler(
	movementRepository MovementRepository,
	exerciseRepository ExerciseRepository,
) *ExerciseHandler {

	return &ExerciseHandler{
		movementRepository: movementRepository,
		exerciseRepository: exerciseRepository,
	}
}

func (h *ExerciseHandler) GetMovementsHandler(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	patientID := p.ByName("patientId")

	movements, err := h.movementRepository.FindByPatient(patientID)
	if err != nil {

		writeInternalServerError(w, err.Error())
		return
	}

	movementsResp := make([]movement, len(movements))
	for i := range movements {

		results := encodeMovementResults(movements[i].Results)

		movementsResp[i] = movement{
			PatientID:  movements[i].PatientID,
			ExerciseID: movements[i].ExerciseID,
			Repetition: movements[i].Repetition,
			Motion:     movements[i].Motion,
			Status:     movements[i].Status,
			StartedAt:  movements[i].CreatedAt,
			Results:    results,
		}
	}

	writeOk(w, movementsResp)
}

func encodeMovementResults(r []domain.MovementResult) []movementResult {

	movementResults := make([]movementResult, len(r))
	for i := range r {
		movementResults[i] = movementResult{
			Name:   r[i].Name,
			Value:  r[i].Value,
			Status: r[i].Status,
		}
	}

	return movementResults
}

func (h *ExerciseHandler) GetExercise(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	id := p.ByName("id")

	e, err := h.exerciseRepository.FindById(id)
	if err != nil {
		writeInternalServerError(w, err.Error())
		return
	}

	exerciseThresholds := encodeExerciseThreshold(e.Threshold)

	resp := exercise{
		ID:              e.ID,
		Name:            e.Name,
		Repetitions:     e.Repetitions,
		ExpectedMotions: e.ExpectedMotion,
		Thresholds:      exerciseThresholds,
	}

	writeOk(w, resp)
}

func encodeExerciseThreshold(et []domain.ExerciseThreshold) []exerciseThreshold {

	exerciseThresholds := make([]exerciseThreshold, len(et))
	for i := range et {
		exerciseThresholds[i] = exerciseThreshold{
			Name:  et[i].Name,
			Value: et[i].Value,
		}
	}

	return exerciseThresholds
}

func (h *ExerciseHandler) CreateExercise(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var requestBody exercise
	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		writeBadRequest(w, "invalid request payload")
		return
	}

	ex := domain.Exercise{
		ID:             requestBody.ID,
		Name:           requestBody.Name,
		Repetitions:    requestBody.Repetitions,
		ExpectedMotion: requestBody.ExpectedMotions,
		Threshold:      decodeExerciseThresholds(requestBody.Thresholds),
	}

	if err := h.exerciseRepository.Save(ex); err != nil {
		writeInternalServerError(w, err.Error())
		return
	}

	writeCreated(w, map[string]string{})
}

func decodeExerciseThresholds(thresholds []exerciseThreshold) []domain.ExerciseThreshold {

	data := make([]domain.ExerciseThreshold, len(thresholds))
	for i := range thresholds {

		data[i] = domain.ExerciseThreshold{
			Name:  thresholds[i].Name,
			Value: thresholds[i].Value,
		}
	}

	return data
}
