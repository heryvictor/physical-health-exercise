package rest

import (
	"encoding/json"
	"net/http"
	"physical-health/domain"
	"strings"

	"github.com/julienschmidt/httprouter"
)

//go:generate mockgen -destination=mocks/patient_repository_mock.go -package=mocks . PatientRepository
type PatientRepository interface {
	FindAll() ([]domain.Patient, error)
	Save(domain.Patient) error
}

type PatientHandler struct {
	patientRepository PatientRepository
}

func NewPatientHandler(t PatientRepository) *PatientHandler {

	return &PatientHandler{
		patientRepository: t,
	}
}

func (h *PatientHandler) GetPatientsHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	patients, err := h.patientRepository.FindAll()
	if err != nil {

		writeInternalServerError(w, err.Error())
		return
	}

	type response struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	patientsResponse := make([]response, len(patients))
	for i := range patients {

		patientsResponse[i] = response{
			ID:   patients[i].ID,
			Name: patients[i].Name,
		}
	}

	writeOk(w, patientsResponse)
}

// GetPatientsHandler ...
func (h *PatientHandler) NewPatientHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var requestBody struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		writeBadRequest(w, "invalid request payload")
		return
	}

	if strings.TrimSpace(requestBody.ID) == "" {
		writeBadRequest(w, "The attribute patient.id should not be empty")
		return
	}

	if strings.TrimSpace(requestBody.Name) == "" {
		writeBadRequest(w, "The attribute patient.name should not be empty")
		return
	}

	t := domain.Patient{ID: requestBody.ID, Name: requestBody.Name}

	err := h.patientRepository.Save(t)
	if err != nil {

		writeInternalServerError(w, err.Error())
		return
	}

	type patientResponse struct {
		ID string `json:"id"`
	}

	writeCreated(w, patientResponse{ID: t.ID})
}
