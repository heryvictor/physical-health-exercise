package rest

import (
	"encoding/json"
	"net/http"
	"physical-health/domain"
	"time"

	"github.com/julienschmidt/httprouter"
)

//go:generate mockgen -destination=mocks/track_motion_publisher_mock.go -package=mocks . Publisher
type Publisher interface {
	Publish(string, string, domain.ConsumerMessage) error
}

type TrackMotionHandler struct {
	publisher Publisher
}

func NewTrackMotionHandler(publisher Publisher) *TrackMotionHandler {

	return &TrackMotionHandler{
		publisher: publisher,
	}
}

func (h *TrackMotionHandler) RegisterTrackMotionHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var requestBody struct {
		PatientID  string    `json:"patientId"`
		ExerciseID string    `json:"exerciseId"`
		Repetition int       `json:"repetition"`
		CreatedAt  time.Time `json:"createdAt"`
		Motion     []float64 `json:"motion"`
	}

	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		writeBadRequest(w, "invalid request payload")
		return
	}

	trackMotion := domain.TrackMotion{
		PatientID:  requestBody.PatientID,
		ExerciseID: requestBody.ExerciseID,
		Repetition: requestBody.Repetition,
		CreatedAt:  requestBody.CreatedAt,
		Motion:     requestBody.Motion,
	}

	message, err := json.Marshal(trackMotion)
	if err != nil {
		writeBadRequest(w, "unable to encode track motion "+err.Error())
		return
	}

	err = h.publisher.Publish(domain.TrackMotionEventName, trackMotion.PatientID, message)

	if err != nil {
		writeInternalServerError(w, "unable to process the track motion "+err.Error())
		return
	}

	writeCreated(w, map[string]string{})
}
