package rest

import (
	"encoding/json"
	"fmt"
	"runtime/debug"

	"net/http"
	"physical-health/infrastructure"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const (
	AppName = "physical-health-app"

	errorMessageBody = `{"message": "%s"}`
	contentType      = "Content-Type"
	applicationJson  = "application/json"
)

// Server structs that contains all http handle routes
type Rest struct {
	logger             infrastructure.Logger
	version            string
	therapistHandler   *TherapistHandler
	trackMotionHandler *TrackMotionHandler
	patientHandler     *PatientHandler
	exerciseHandler    *ExerciseHandler
}

func New(logger infrastructure.Logger, version string) *Rest {

	return &Rest{
		logger:  logger,
		version: version,
	}
}

func (r *Rest) SetTrackMotionHandler(tm *TrackMotionHandler) *Rest {

	r.trackMotionHandler = tm
	return r
}

func (r *Rest) SetTherapistHandler(t *TherapistHandler) *Rest {

	r.therapistHandler = t
	return r
}

func (r *Rest) SetPatientHandler(p *PatientHandler) *Rest {

	r.patientHandler = p
	return r
}

func (r *Rest) SetExerciseHandler(e *ExerciseHandler) *Rest {

	r.exerciseHandler = e
	return r
}

func (r *Rest) NewHandler() http.Handler {

	router := httprouter.New()

	router.GET("/metrics", promAdapterHandler(promhttp.Handler()))
	router.HandlerFunc("GET", "/api", showVersion(r.version))

	router.GET("/api/therapists", r.therapistHandler.GetTherapistsHandler)
	router.POST("/api/therapists", r.therapistHandler.NewTherapistHandler)

	router.GET("/api/patients", r.patientHandler.GetPatientsHandler)
	router.POST("/api/patients", r.patientHandler.NewPatientHandler)
	router.GET("/api/patients/:patientId/exercises", r.exerciseHandler.GetMovementsHandler)

	router.POST("/api/exercises", r.exerciseHandler.CreateExercise)
	router.GET("/api/exercises/:id", r.exerciseHandler.GetExercise)

	router.POST("/api/track-motions", r.trackMotionHandler.RegisterTrackMotionHandler)

	router.PanicHandler = r.panicHandler

	return router
}

func promAdapterHandler(h http.Handler) httprouter.Handle {

	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

		h.ServeHTTP(w, r)
	}
}

func showVersion(version string) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {

		type infoResponse struct {
			Name    string `json:"name"`
			Version string `json:"version"`
		}

		writeOk(w, &infoResponse{
			Name:    AppName,
			Version: version,
		})
	}
}

func (r *Rest) panicHandler(w http.ResponseWriter, req *http.Request, e interface{}) {

	r.logger.Error("panic: %+v", e)
	r.logger.Error("stacktrace error: %s", string(debug.Stack()))

	writeInternalServerError(w, fmt.Sprintf("%v", e))
}

func writeCreated(w http.ResponseWriter, content interface{}) {

	respContent, err := json.Marshal(content)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set(contentType, applicationJson)
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(respContent)
}

func writeOk(w http.ResponseWriter, content interface{}) {

	respContent, err := json.Marshal(content)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set(contentType, applicationJson)

	w.WriteHeader(http.StatusOK)
	_, _ = w.Write(respContent)
}

func writeInternalServerError(w http.ResponseWriter, message string) {

	w.Header().Set(contentType, applicationJson)
	w.WriteHeader(http.StatusInternalServerError)
	_, _ = fmt.Fprintf(w, errorMessageBody, message)
}

func writeNotFound(w http.ResponseWriter) {

	w.Header().Set(contentType, applicationJson)
	w.WriteHeader(http.StatusNotFound)
	_, _ = fmt.Fprintf(w, errorMessageBody, "not found")
}

func writeBadRequest(w http.ResponseWriter, message string) {

	w.Header().Set(contentType, applicationJson)
	w.WriteHeader(http.StatusBadRequest)
	_, _ = fmt.Fprintf(w, errorMessageBody, message)
}
