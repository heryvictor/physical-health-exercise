package rest_test

import (
	"bytes"
	"encoding/json"
	"errors"
	"physical-health/entrypoint/rest/mocks"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/golang/mock/gomock"

	"physical-health/domain"
	"physical-health/entrypoint/rest"
	"physical-health/infrastructure"

	"github.com/stretchr/testify/assert"
)

func TestGetTherapists_WithSuccess(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockTherapistService(mockCtrl)

	logger := infrastructure.NewDefaultLogger()
	therapistHandler := rest.NewTherapistHandler(mockService)
	rest := rest.New(logger, "1").SetTherapistHandler(therapistHandler)

	therapists := []domain.Therapist{
		{ID: "1", Name: "Therapist 1"},
		{ID: "2", Name: "Therapist 2"},
	}

	mockService.EXPECT().List().Return(therapists, nil).Times(1)

	httpServer := httptest.NewServer(rest.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	t.Log(url)
	url.Path = "/api/therapists"

	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode, "The expected status is OK.")

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	type response struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	var result []response
	err = json.Unmarshal(respBody, &result)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, 2, len(result))
	assert.Equal(t, "Therapist 1", result[0].Name)
	assert.Equal(t, "Therapist 2", result[1].Name)
}

func TestGetUsers_WithServerError(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockTherapistService(mockCtrl)

	logger := infrastructure.NewDefaultLogger()
	therapistHandler := rest.NewTherapistHandler(mockService)
	rest := rest.New(logger, "1").SetTherapistHandler(therapistHandler)

	mockService.EXPECT().List().Return(nil, errors.New("test error")).Times(1)

	httpServer := httptest.NewServer(rest.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	t.Log(url)
	url.Path = "/api/therapists"

	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode, "The expected status is InternalServerError.")

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	var errorResponse struct {
		Message string `json:"message"`
	}

	err = json.Unmarshal(respBody, &errorResponse)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "test error", errorResponse.Message)
}

func TestNewUser_WithSuccess(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockTherapistService(mockCtrl)

	logger := infrastructure.NewDefaultLogger()
	therapistHandler := rest.NewTherapistHandler(mockService)
	rest := rest.New(logger, "1").SetTherapistHandler(therapistHandler)

	mockService.EXPECT().Create(gomock.Any()).Return(nil).Times(1)

	httpServer := httptest.NewServer(rest.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	t.Log(url)
	url.Path = "/api/therapists"

	requestBody, _ := json.Marshal(map[string]string{
		"id":   "x1",
		"name": "Created Therapists",
	})

	req, err := http.NewRequest("POST", url.String(), bytes.NewBuffer(requestBody))
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusCreated, resp.StatusCode, "The expected status is Created.")

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	var responseBody struct {
		ID string `json:"id"`
	}

	err = json.Unmarshal(respBody, &responseBody)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "x1", responseBody.ID)
}

func TestNewUser_WithServerError(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockTherapistService(mockCtrl)

	logger := infrastructure.NewDefaultLogger()
	therapistHandler := rest.NewTherapistHandler(mockService)
	rest := rest.New(logger, "1").SetTherapistHandler(therapistHandler)

	mockService.EXPECT().Create(gomock.Any()).Return(errors.New("test error")).Times(1)

	httpServer := httptest.NewServer(rest.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	t.Log(url)
	url.Path = "/api/therapists"

	requestBody, _ := json.Marshal(map[string]string{
		"id":   "x1",
		"name": "Created Therapist",
	})

	req, err := http.NewRequest("POST", url.String(), bytes.NewBuffer(requestBody))
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode, "The expected status is Internal Server Error.")

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	var responseBody struct {
		Message string `json:"message"`
	}

	err = json.Unmarshal(respBody, &responseBody)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "test error", responseBody.Message)
}

func TestNewTherapist_WithInvalidPayloads(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockTherapistService(mockCtrl)

	logger := infrastructure.NewDefaultLogger()
	therapistHandler := rest.NewTherapistHandler(mockService)
	rest := rest.New(logger, "1").SetTherapistHandler(therapistHandler)

	mockService.EXPECT().Create(gomock.Any()).Return(nil).AnyTimes()

	httpServer := httptest.NewServer(rest.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	if err != nil {
		t.Fatal(err)
	}

	t.Log(url)
	url.Path = "/api/therapists"

	type test struct {
		name            string
		requestBody     map[string]interface{}
		expectedMessage string
	}

	tableTest := []test{
		{
			name:            "Invalid Therapist.id",
			requestBody:     map[string]interface{}{"id": "", "name": "Created Therapist"},
			expectedMessage: "The attribute therapist.id should not be empty",
		},
		{
			name:            "Invalid Therapist.name",
			requestBody:     map[string]interface{}{"id": "x1", "name": ""},
			expectedMessage: "The attribute therapist.name should not be empty",
		},
		{
			name:            "Invalid Payload",
			requestBody:     map[string]interface{}{"name": []string{"x", "y"}},
			expectedMessage: "invalid request payload",
		},
	}

	for i := range tableTest {

		t.Run(tableTest[i].name, func(t *testing.T) {

			requestBody, _ := json.Marshal(tableTest[i].requestBody)

			req, err := http.NewRequest("POST", url.String(), bytes.NewBuffer(requestBody))
			if err != nil {
				t.Fatal(err)
			}

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, http.StatusBadRequest, resp.StatusCode, "The expected status is BadRequest.")

			respBody, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Fatal(err)
			}

			var responseBody struct {
				Message string `json:"message"`
			}

			err = json.Unmarshal(respBody, &responseBody)
			if err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, tableTest[i].expectedMessage, responseBody.Message)
		})
	}

}
