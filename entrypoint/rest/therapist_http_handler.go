package rest

import (
	"encoding/json"
	"net/http"
	"physical-health/domain"
	"strings"

	"github.com/julienschmidt/httprouter"
)

//go:generate mockgen -destination=mocks/therapist_service_mock.go -package=mocks . TherapistService
type TherapistService interface {
	List() ([]domain.Therapist, error)
	Create(domain.Therapist) error
}

type TherapistHandler struct {
	therapistService TherapistService
}

func NewTherapistHandler(t TherapistService) *TherapistHandler {

	return &TherapistHandler{
		therapistService: t,
	}
}

func (h *TherapistHandler) GetTherapistsHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	therapists, err := h.therapistService.List()
	if err != nil {

		writeInternalServerError(w, err.Error())
		return
	}

	type response struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	therapistsResponse := make([]response, len(therapists))
	for i := range therapists {

		therapistsResponse[i] = response{
			ID:   therapists[i].ID,
			Name: therapists[i].Name,
		}
	}

	writeOk(w, therapistsResponse)
}

// GetTherapistsHandler ...
func (h *TherapistHandler) NewTherapistHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var requestBody struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	if err := json.NewDecoder(r.Body).Decode(&requestBody); err != nil {
		writeBadRequest(w, "invalid request payload")
		return
	}

	if strings.TrimSpace(requestBody.ID) == "" {
		writeBadRequest(w, "The attribute therapist.id should not be empty")
		return
	}

	if strings.TrimSpace(requestBody.Name) == "" {
		writeBadRequest(w, "The attribute therapist.name should not be empty")
		return
	}

	t := domain.Therapist{ID: requestBody.ID, Name: requestBody.Name}

	err := h.therapistService.Create(t)
	if err != nil {

		writeInternalServerError(w, err.Error())
		return
	}

	type therapistResponse struct {
		ID string `json:"id"`
	}

	writeCreated(w, therapistResponse{ID: t.ID})
}
