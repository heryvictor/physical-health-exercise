package rest_test

import (
	"encoding/json"
	"physical-health/entrypoint/rest"
	"physical-health/infrastructure"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetVersion_WithSuccess(t *testing.T) {

	logger := infrastructure.NewDefaultLogger()
	server := rest.New(logger, "v1.0")

	httpServer := httptest.NewServer(server.NewHandler())
	defer httpServer.Close()

	url, err := url.Parse(httpServer.URL)
	t.Log(url)
	url.Path = "/api"

	req, err := http.NewRequest("GET", url.String(), nil)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, http.StatusOK, resp.StatusCode, "The expected status is OK.")

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	var responseBody struct {
		Name    string `json:"name"`
		Version string `json:"version"`
	}

	err = json.Unmarshal(respBody, &responseBody)
	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, "v1.0", responseBody.Version)
	assert.Equal(t, rest.AppName, responseBody.Name)
}
