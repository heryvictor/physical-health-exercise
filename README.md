# Practical exercise App

### Commands

The project uses a Makefile [Make](https://www.gnu.org/software/make/) that contains all useful commands that you need to build, test and run.

### Help

Access the help of all existent commands. Just type make in your terminal, on the root directory of the project.

```bash
$ make
```

### Go required Tools

Before start to develop, run the following command to check if all required tools are installed. Also, please add the required binaries in your PATH.
Currently, this command will install the Go coverage tools through **go get**

```bash
$ make tools
```

### Running the application with - Go Run

This command will run the application with **go run** tool. The application will start listen on Localhost and port 8090 by default

```bash
$ make compose-up
$ make run
```

### Running tests

```bash
$ make test
```

### Running tests with coverage

```bash
$ make coverage
```

After that, you can open the [Coverage Report](./report/coverage.html) in Html

### Build the executables in both Linux and Mac os architectures

```bash
$ make build
```

### All in targets to test, generate report and build the executables

```bash
$ make all
```
