import { check } from 'k6';
import http from 'k6/http';

export default function () {
  const url = 'http://localhost:8090/api/track-motions';
  const payload = JSON.stringify({
        patientId: "123",
        exerciseId: "456",
        repetition: 3,
        createdAt: "2021-07-14T15:55:12Z",
        motion: [1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 32.0, 21.0, 13.0, 8.0, 5.0,
          3.0, 2.0, 1.0]
      }
  );

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let res = http.post(url, payload, params);

  check(res, {
    'is status 201': (r) => r.status === 201,
  });

}
