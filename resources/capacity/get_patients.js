import { check } from 'k6';
import http from 'k6/http';

export default function () {
  const url = 'http://localhost:8090/api/patients';

  var params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  let res = http.get(url, params);
  check(res, {
    'is status 200': (r) => r.status === 200,
  });
}
