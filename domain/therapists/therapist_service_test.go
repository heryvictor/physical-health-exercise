package therapists

import (
	"physical-health/domain"
	"physical-health/domain/therapists/mocks"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

func TestTherapistService_List(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepository := mocks.NewMockTherapistRepository(mockCtrl)

	therapistService := NewTherapistService(mockRepository)

	therapists := []domain.Therapist{
		{ID: "1", Name: "Therapist 1"},
		{ID: "2", Name: "Therapist 2"},
	}

	mockRepository.EXPECT().GetAll().Return(therapists, nil).Times(1)

	returnedTherapists, err := therapistService.List()

	assert.Nil(t, err)
	assert.Equal(t, 2, len(returnedTherapists))
}

func TestTherapistService_Create(t *testing.T) {

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepository := mocks.NewMockTherapistRepository(mockCtrl)

	therapistService := NewTherapistService(mockRepository)

	therapist := domain.Therapist{ID: "1", Name: "Therapist 1"}

	mockRepository.EXPECT().Insert(therapist).Return(nil).Times(1)

	err := therapistService.Create(therapist)

	assert.Nil(t, err)
}
