package therapists

import "physical-health/domain"

//go:generate mockgen -destination=mocks/therapist_repository_mock.go -package=mocks . TherapistRepository
type TherapistRepository interface {
	GetAll() ([]domain.Therapist, error)
	Insert(domain.Therapist) error
}

type TherapistService struct {
	therapistRepository TherapistRepository
}

func NewTherapistService(therapistRepository TherapistRepository) *TherapistService {

	return &TherapistService{
		therapistRepository: therapistRepository,
	}
}

func (t *TherapistService) List() ([]domain.Therapist, error) {

	return t.therapistRepository.GetAll()
}

func (t *TherapistService) Create(therapist domain.Therapist) error {

	return t.therapistRepository.Insert(therapist)
}
