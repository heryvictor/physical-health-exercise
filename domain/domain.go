package domain

import "time"

const TrackMotionEventName = "track_motions"

type (
	Therapist struct {
		ID   string
		Name string
	}

	Patient struct {
		ID   string
		Name string
	}

	Exercise struct {
		ID             string
		Name           string
		Repetitions    int
		ExpectedMotion []float64
		Threshold      []ExerciseThreshold
	}

	ExerciseThreshold struct {
		Name  string
		Value float64
	}

	TrackMotion struct {
		PatientID  string
		ExerciseID string
		Repetition int
		CreatedAt  time.Time
		Motion     []float64
	}

	Movement struct {
		PatientID  string
		ExerciseID string
		Repetition int
		CreatedAt  time.Time
		Motion     []float64
		Status     string
		Results    []MovementResult
	}

	MovementResult struct {
		Name   string
		Status string
		Value  float64
	}

	ConsumerMessage []byte
)
