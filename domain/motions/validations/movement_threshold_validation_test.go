package validations_test

import (
	"fmt"
	"physical-health/domain"
	"physical-health/domain/motions"
	"physical-health/domain/motions/validations"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidateMovementThreshold_GivenThresholdReached_ReturnsNotOk(t *testing.T) {

	expected := []float64{1.0, 2.0, 3.0, 4.0}
	current := []float64{0.5, 1.0, 1.5, 2.0}

	movementThreshold := &domain.ExerciseThreshold{
		Name:  motions.MovementThresholdName,
		Value: 2.73,
	}

	threshold := validations.ValidateMovementThreshold(movementThreshold, expected, current)

	assert.NotNil(t, threshold)
	assert.Equal(t, "2.74", fmt.Sprintf("%.2f", threshold.Value))
	assert.Equal(t, validations.StatusFAILED, threshold.Status)
}

func TestValidateMovementThreshold_ExpectedAndCurrentHaveSameValues_ReturnsOk(t *testing.T) {

	expected := []float64{1.0, 2.0, 3.0, 4.0}
	current := []float64{1.0, 2.0, 3.0, 4.0}

	movementThreshold := &domain.ExerciseThreshold{
		Name:  motions.MovementThresholdName,
		Value: 0,
	}

	threshold := validations.ValidateMovementThreshold(movementThreshold, expected, current)

	assert.NotNil(t, threshold)
	assert.Equal(t, "0.00", fmt.Sprintf("%.2f", threshold.Value))
	assert.Equal(t, validations.StatusOK, threshold.Status)
}
