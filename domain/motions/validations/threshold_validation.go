package validations

const (
	StatusOK     = "OK"
	StatusFAILED = "FAILED"
)

func maxLen(x, y int) int {

	if x < y {
		return y
	}

	return x
}

func valueAtIndexOrZero(values []float64, index int) float64 {

	if index < len(values) {

		return values[index]
	}

	return 0
}
