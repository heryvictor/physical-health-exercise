package validations

import (
	"math"
	"physical-health/domain"
)

// ValidateMovementThreshold calculate the difference between expected and executed movement by using Euclidean Difference
func ValidateMovementThreshold(threshold *domain.ExerciseThreshold, expected, executed []float64) domain.MovementResult {

	movementDifference := calculateEuclideanDifference(expected, executed)

	status := StatusOK
	if movementDifference > threshold.Value {
		status = StatusFAILED
	}

	return domain.MovementResult{
		Name:   threshold.Name,
		Status: status,
		Value:  movementDifference,
	}
}

func calculateEuclideanDifference(current, expected []float64) float64 {

	maxLen := maxLen(len(current), len(expected))
	total := 0.0

	for i := 0; i < maxLen; i++ {

		e := valueAtIndexOrZero(expected, i)
		c := valueAtIndexOrZero(current, i)

		total += math.Pow(e-c, 2)
	}

	return math.Sqrt(total)
}
