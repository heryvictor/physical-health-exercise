package validations

import (
	"physical-health/domain"
	"sort"
)

// ValidateHighIntensityThreshold calculate the difference between expected and executed movement by using Euclidean Difference
func ValidateHighIntensityThreshold(threshold *domain.ExerciseThreshold, expected, executed []float64) domain.MovementResult {

	movementDifference := top3HighIntensity(expected, executed)

	status := StatusOK
	if movementDifference > threshold.Value {
		status = StatusFAILED
	}

	return domain.MovementResult{
		Name:   threshold.Name,
		Status: status,
		Value:  movementDifference,
	}
}

func top3HighIntensity(expected, current []float64) float64 {

	c := make([]float64, len(current))
	e := make([]float64, len(expected))

	copy(c, current)
	copy(e, expected)

	sort.Float64s(c)
	sort.Float64s(e)

	var total float64
	for i := 3; i > 0; i-- {

		total += valueAtIndexOrZero(e, len(e)-i) - valueAtIndexOrZero(c, len(c)-i)
	}

	return total
}
