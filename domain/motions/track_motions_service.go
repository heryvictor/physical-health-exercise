package motions

import (
	"context"
	"encoding/json"
	"physical-health/domain"
	"physical-health/domain/motions/validations"
	"physical-health/infrastructure"
)

//go:generate mockgen -destination=mocks/track_motion_consumer_mock.go -package=mocks . Consumer
type Consumer interface {
	ConsumeTopic(context.Context, string) <-chan domain.ConsumerMessage
}

//go:generate mockgen -destination=mocks/movement_repository_mock.go -package=mocks . MovementRepository
type MovementRepository interface {
	Save(domain.Movement) error
}

//go:generate mockgen -destination=mocks/exercise_repository_mock.go -package=mocks . ExerciseRepository
type ExerciseRepository interface {
	FindById(string) (*domain.Exercise, error)
}

const (
	MovementThresholdName      = "movementThreshold"
	HighIntensityThresholdName = "highIntensityThreshold"
)

type TrackMotionsService struct {
	consumer           Consumer
	movementRepository MovementRepository
	exerciseRepository ExerciseRepository
	logger             infrastructure.Logger
}

func NewTrackMotionsService(
	consumer Consumer,
	movementRepository MovementRepository,
	exerciseRepository ExerciseRepository,
	logger infrastructure.Logger,
) *TrackMotionsService {

	return &TrackMotionsService{
		consumer:           consumer,
		movementRepository: movementRepository,
		exerciseRepository: exerciseRepository,
		logger:             logger,
	}
}

func (s *TrackMotionsService) Start(ctx context.Context) {

	messageChan := s.consumer.ConsumeTopic(ctx, domain.TrackMotionEventName)

	go func(mChan <-chan domain.ConsumerMessage) {

		for m := range mChan {

			trackMotion, err := decodeMessage(m)
			if err != nil {
				s.logger.Error("invalid track motion message: %v", err.Error())
			}

			err = s.ProcessTrackMotion(trackMotion)
			if err != nil {
				s.logger.Error("track motion processed with error: %v", err.Error())
			}
		}

	}(messageChan)

}

func decodeMessage(m domain.ConsumerMessage) (*domain.TrackMotion, error) {

	var trackMotion domain.TrackMotion
	if err := json.Unmarshal(m, &trackMotion); err != nil {
		return nil, err
	}
	return &trackMotion, nil
}

func (s *TrackMotionsService) ProcessTrackMotion(trackMotion *domain.TrackMotion) error {

	movement := domain.Movement{
		PatientID:  trackMotion.PatientID,
		ExerciseID: trackMotion.ExerciseID,
		Repetition: trackMotion.Repetition,
		CreatedAt:  trackMotion.CreatedAt,
		Motion:     trackMotion.Motion,
	}

	exercise, err := s.exerciseRepository.FindById(movement.ExerciseID)
	if err != nil {
		return err
	}

	movement.Results = make([]domain.MovementResult, 0)

	movementThreshold := findExerciseThreshold(exercise, MovementThresholdName)
	if movementThreshold != nil {
		t := validations.ValidateMovementThreshold(movementThreshold, exercise.ExpectedMotion, movement.Motion)
		movement.Results = append(movement.Results, t)
	}

	highIntensityThreshold := findExerciseThreshold(exercise, HighIntensityThresholdName)
	if highIntensityThreshold != nil {
		t := validations.ValidateHighIntensityThreshold(highIntensityThreshold, exercise.ExpectedMotion, movement.Motion)
		movement.Results = append(movement.Results, t)
	}

	movement.Status = calculateStatus(movement.Results)

	if err := s.movementRepository.Save(movement); err != nil {
		return err
	}

	return nil
}

func calculateStatus(results []domain.MovementResult) string {

	for i := range results {
		if results[i].Status == validations.StatusFAILED {
			return validations.StatusFAILED
		}
	}

	return validations.StatusOK
}

func findExerciseThreshold(exercise *domain.Exercise, name string) *domain.ExerciseThreshold {

	for _, t := range exercise.Threshold {
		if name == t.Name {
			return &t
		}
	}

	return nil
}
