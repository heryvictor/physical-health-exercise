.PHONY: help
help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sed -e 's/.*Makefile://' | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

include docker/Makefile

VERSION=v0.0.1

GO_TOOLS = \
	github.com/axw/gocov/gocov \
	github.com/matm/gocov-html

# Check if the required tools are in PATH of the your local environment
EXECUTABLES = go gocov gocov-html
CHECK_FUNCTIONS = $(foreach exec,$(EXECUTABLES),$(if $(shell which $(exec)),,$(error "Executable $(exec) not found in your PATH)))

check:
	@exec $(CHECK_FUNCTIONS)

.PHONY: tools
tools: ## Install godep and cover tools
	go get -u -v $(GO_TOOLS)

.PHONY: generate-mocks
generate-mocks: ## Generate mocks
	go generate ./...

.PHONY: test
test: check generate-mocks ## run the tests
	go test ./... -v -race -tags unit

.PHONY: bench
bench: check ## run the benchmark tests
	go test ./... -v --bench=. -run=XXX --benchmem -benchtime 1000x

.PHONY: clean
clean: ## clean build, report and generated mock folders
	@-rm -rf build/*
	@-rm -rf report/*
	@-find ./ -name "*_mock.go" | xargs rm -r

.PHONY: coverage
coverage: check generate-mocks ## run the coverage report
	mkdir -p report/
	go test ./... -tags unit -coverprofile report/coverage.out
	gocov convert report/coverage.out | gocov-html > report/coverage.html

.PHONY: run
run: check ## run the application
	go run -ldflags "-X main.version=$(VERSION)" command/api/main.go

.PHONY: build-linux
build-linux: check
	mkdir -p build
	CGO_ENABLED=0 GOOS=linux go build -ldflags "-X main.version=$(VERSION)" -v -a -installsuffix cgo  -o build/physical-health-linux-x86-64 ./command/api

.PHONY: build-darwin
build-darwin: check
	mkdir -p build
	CGO_ENABLED=0 GOOS=darwin go build -ldflags "-X main.version=$(VERSION)" -v -a -installsuffix cgo  -o build/physical-health-darwin-x86-64 ./command/api

.PHONY: build
build: check build-linux build-darwin ## build both linux and darwin arch

.PHONY: gomod-tidy
gomod-tidy: check ## golang go mod tidy in order to clean up unnecessary dependencies
	go mod tidy

.PHONY: all # All tasks - clean up, tests, coverage and build
all: clean gomod-tidy generate-mocks coverage build
