package infrastructure

import (
	"fmt"
	"os"
)

type Logger interface {
	Info(string, ...interface{})
	Error(string, ...interface{})
}

type defaultLogger struct{}

func NewDefaultLogger() Logger {

	return &defaultLogger{}
}

func (d *defaultLogger) Info(format string, v ...interface{}) {

	fmt.Fprintf(os.Stdout, format, v...)
	fmt.Fprintln(os.Stdout)
}

func (d *defaultLogger) Error(format string, v ...interface{}) {

	fmt.Fprintf(os.Stderr, format, v...)
	fmt.Fprintln(os.Stdout)
}
