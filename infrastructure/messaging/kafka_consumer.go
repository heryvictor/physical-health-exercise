package messaging

import (
	"context"
	"fmt"
	"physical-health/domain"
	"physical-health/infrastructure"

	"github.com/segmentio/kafka-go"
)

const consumerGroupSuffix = "app_consumer_group"

type KafkaConsumer struct {
	uri    string
	logger infrastructure.Logger
}

func NewKafkaConsumer(uri string, logger infrastructure.Logger) *KafkaConsumer {

	return &KafkaConsumer{
		uri:    uri,
		logger: logger,
	}
}

func (c *KafkaConsumer) ConsumeTopic(ctx context.Context, topicName string) <-chan domain.ConsumerMessage {

	consumerGroupName := fmt.Sprintf("%s_%s", topicName, consumerGroupSuffix)

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{c.uri},
		GroupID:  consumerGroupName,
		Topic:    topicName,
		MinBytes: 10e3, // 10KB
		MaxBytes: 10e6, // 10MB
	})

	messageChan := make(chan domain.ConsumerMessage)

	go func() {

		defer close(messageChan)

		for {
			m, err := r.ReadMessage(ctx)
			if err != nil {
				c.logger.Error("kafka consumer of topic [%s] has finished with message: %s", topicName, err)
				break
			}

			c.logger.Info("kafka message consumed on topic: %s, partition: %d, offset: %d, key: %v, value: %s",
				m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))

			messageChan <- m.Value
		}

		if err := r.Close(); err != nil {
			c.logger.Error("close kafka consumer of topic [%s] with error: %s", topicName, err)
		}
	}()

	return messageChan
}
