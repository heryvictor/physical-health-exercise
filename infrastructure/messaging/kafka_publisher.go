package messaging

import (
	"context"
	"physical-health/domain"
	"time"

	"github.com/pkg/errors"

	"github.com/segmentio/kafka-go"
)

type KafkaPublisher struct {
	uri string
}

func NewKafkaPublisher(uri string) *KafkaPublisher {

	return &KafkaPublisher{
		uri: uri,
	}
}

func (p *KafkaPublisher) Publish(topicName, key string, message domain.ConsumerMessage) error {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	writer := &kafka.Writer{
		Addr:         kafka.TCP(p.uri),
		Topic:        topicName,
		Balancer:     &kafka.LeastBytes{},
		Async:        false,
		BatchSize:    1,
		RequiredAcks: kafka.RequireOne,
	}

	defer writer.Close()

	kafkaMessage := kafka.Message{
		Key:     []byte(key),
		Value:   message,
		Headers: []kafka.Header{},
	}

	err := writer.WriteMessages(ctx, kafkaMessage)
	if err != nil {
		return errors.Wrap(err, "failed to write message to topic")
	}

	return nil
}
