package repository_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"physical-health/infrastructure/repository"
	"testing"
	"time"
)

const (
	defaultMongoURL = "mongodb://localhost:27017"
	defaultMongoUsername = "mongo"
	defaultMongoPassword = "mongopass"
)

func cleanCollection(
	t *testing.T,
	instance *repository.MongoInstance,
	database, collectionName string) {

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	client, err := instance.NewClient(ctx)
	assert.NoError(t, err)

	defer client.Disconnect(ctx)

	coll := client.Database(database).Collection(collectionName)

	_, err = coll.DeleteMany(ctx, bson.M{})
	assert.NoError(t, err)
}