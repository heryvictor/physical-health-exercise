package repository

import (
	"context"
	"physical-health/domain"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const ExerciseCollectionName = "exercise"

type (
	ExerciseRepository struct {
		instance *MongoInstance
		database string
	}

	exerciseData struct {
		ID              string                  `bson:"_id"`
		Name            string                  `bson:"name"`
		Repetitions     int                     `bson:"repetitions"`
		ExpectedMotions []float64               `bson:"expectedMotions"`
		Threshold       []exerciseThresholdData `bson:"thresholds"`
	}

	exerciseThresholdData struct {
		Name  string  `bson:"name"`
		Value float64 `bson:"value"`
	}
)

func NewExerciseRepository(instance *MongoInstance) *ExerciseRepository {

	return &ExerciseRepository{
		instance: instance,
		database: databaseName,
	}
}

func (r *ExerciseRepository) SetDatabaseName(databaseName string) {

	r.database = databaseName
}

func (r *ExerciseRepository) FindById(id string) (*domain.Exercise, error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(r.database).Collection(ExerciseCollectionName)

	var exerciseData exerciseData
	err = coll.FindOne(ctx, bson.D{{"_id", id}}).Decode(&exerciseData)

	if err != nil {
		return nil, err
	}

	return &domain.Exercise{
		ID:             exerciseData.ID,
		Name:           exerciseData.Name,
		Repetitions:    exerciseData.Repetitions,
		ExpectedMotion: exerciseData.ExpectedMotions,
		Threshold:      decodeExerciseThresholds(exerciseData.Threshold),
	}, nil
}

func decodeExerciseThresholds(thresholds []exerciseThresholdData) []domain.ExerciseThreshold {

	if thresholds == nil {
		return make([]domain.ExerciseThreshold, 0)
	}

	data := make([]domain.ExerciseThreshold, len(thresholds))
	for i := range thresholds {

		data[i] = domain.ExerciseThreshold{
			Name:  thresholds[i].Name,
			Value: thresholds[i].Value,
		}
	}

	return data
}

func (r *ExerciseRepository) Save(exercise domain.Exercise) (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(r.database).Collection(ExerciseCollectionName)

	opts := options.Update().SetUpsert(true)

	e := exerciseData{
		ID:              exercise.ID,
		Name:            exercise.Name,
		ExpectedMotions: exercise.ExpectedMotion,
		Repetitions:     exercise.Repetitions,
		Threshold:       encodeExerciseThresholds(exercise.Threshold),
	}

	_, err = coll.UpdateOne(ctx, bson.D{{"_id", e.ID}}, bson.D{{"$set", e}}, opts)

	if err != nil {
		return err
	}

	return nil
}

func encodeExerciseThresholds(thresholds []domain.ExerciseThreshold) []exerciseThresholdData {

	if thresholds == nil {
		return make([]exerciseThresholdData, 0)
	}

	data := make([]exerciseThresholdData, len(thresholds))
	for i := range thresholds {

		data[i] = exerciseThresholdData{
			Name:  thresholds[i].Name,
			Value: thresholds[i].Value,
		}
	}

	return data
}
