package repository

import (
	"context"
	"physical-health/domain"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const movementCollectionName = "movement"

type (
	MovementRepository struct {
		instance *MongoInstance
	}

	movementData struct {
		PatientID  string               `bson:"patientId,omitempty"`
		ExerciseID string               `bson:"exerciseId,omitempty"`
		Repetition int                  `bson:"repetition,omitempty"`
		Motion     []float64            `bson:"motion,omitempty"`
		Status     string               `bson:"status,omitempty"`
		Results    []movementResultData `bson:"results,omitempty"`
		StartedAt  time.Time            `bson:"startedAt,omitempty"`
	}

	movementResultData struct {
		Name   string  `bson:"name"`
		Status string  `bson:"status"`
		Value  float64 `bson:"value"`
	}
)

func NewMovementRepository(instance *MongoInstance) *MovementRepository {

	return &MovementRepository{
		instance: instance,
	}
}

func (r *MovementRepository) Save(movement domain.Movement) (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(movementCollectionName)

	_, err = coll.InsertOne(ctx, movementData{
		PatientID:  movement.PatientID,
		ExerciseID: movement.ExerciseID,
		Status:     movement.Status,
		Repetition: movement.Repetition,
		Motion:     movement.Motion,
		Results:    encodeMovementResults(movement.Results),
		StartedAt:  movement.CreatedAt,
	})

	if err != nil {
		return err
	}

	return nil
}

func encodeMovementResults(movementResults []domain.MovementResult) []movementResultData {

	if movementResults == nil {
		return make([]movementResultData, 0)
	}

	data := make([]movementResultData, len(movementResults))
	for i := range movementResults {

		data[i] = movementResultData{
			Name:   movementResults[i].Name,
			Status: movementResults[i].Status,
			Value:  movementResults[i].Value,
		}
	}

	return data
}

func (r *MovementRepository) FindByPatient(patientId string) ([]domain.Movement, error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(movementCollectionName)

	options := options.Find()
	options.SetLimit(10)

	cur, err := coll.Find(ctx, bson.D{{"patientId", patientId}}, options)

	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	movements := make([]domain.Movement, 0)

	for cur.Next(ctx) {

		var m movementData
		err := cur.Decode(&m)
		if err != nil {
			return nil, err
		}

		movements = append(movements, domain.Movement{
			PatientID:  m.PatientID,
			ExerciseID: m.ExerciseID,
			Repetition: m.Repetition,
			CreatedAt:  m.StartedAt,
			Motion:     m.Motion,
			Status:     m.Status,
			Results:    decodeMovementResults(m.Results),
		})
	}

	if err = cur.Err(); err != nil {
		return nil, err
	}

	return movements, nil
}

func decodeMovementResults(movementResults []movementResultData) []domain.MovementResult {

	results := make([]domain.MovementResult, len(movementResults))
	for i := range movementResults {

		results[i] = domain.MovementResult{
			Name:   movementResults[i].Name,
			Status: movementResults[i].Status,
			Value:  movementResults[i].Value,
		}
	}

	return results
}
