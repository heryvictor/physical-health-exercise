package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"time"
)

const (
	databaseName  = "physical-health-exercise"
	authMechanism = "SCRAM-SHA-1"
	defaultCtxTimeout = 3*time.Second
)

type MongoInstance struct {
	uri           string
	username      string
	password      string
	authMechanism string
}

func NewMongoInstance(ctx context.Context, uri string, username string, password string) (*MongoInstance, error) {

	instance := &MongoInstance{
		uri: uri,
		username: username,
		password: password,
		authMechanism: authMechanism,
	}

	ctx, cancel := context.WithTimeout(ctx, defaultCtxTimeout)
	defer cancel()

	client, err := instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	return instance, client.Ping(ctx, readpref.Primary())
}

func (m *MongoInstance) NewClient(ctx context.Context) (*mongo.Client, error) {

	credential := options.Credential{
		AuthMechanism: m.authMechanism,
		Username:      m.username,
		Password:      m.password,
	}

	return mongo.Connect(ctx, options.Client().ApplyURI(m.uri).SetAuth(credential))
}
