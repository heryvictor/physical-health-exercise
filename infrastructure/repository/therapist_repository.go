package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"physical-health/domain"
	"strconv"
)

const therapistCollectionName = "therapist"

type (
	TherapistRepository struct {
		instance *MongoInstance
	}

	therapistData struct {
		ID   int    `bson:"_id,omitempty"`
		Name string `bson:"name,omitempty"`
	}
)

func NewTherapistRepository(instance *MongoInstance) *TherapistRepository {

	return &TherapistRepository{
		instance: instance,
	}
}

func (r *TherapistRepository) GetAll() ([]domain.Therapist, error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(therapistCollectionName)

	cur, err := coll.Find(ctx, bson.D{})

	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	therapists := make([]domain.Therapist, 0)

	for cur.Next(ctx) {

		var u therapistData
		err := cur.Decode(&u)
		if err != nil {
			return nil, err
		}

		therapists = append(therapists, domain.Therapist{
			ID:   strconv.Itoa(u.ID),
			Name: u.Name,
		})
	}

	if err = cur.Err(); err != nil {
		return nil, err
	}

	return therapists, nil
}

// Insert ...
func (r *TherapistRepository) Insert(user domain.Therapist) (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(therapistCollectionName)

	id, err := strconv.Atoi(user.ID)
	if err != nil {
		return err
	}

	_, err = coll.InsertOne(ctx, therapistData{
		ID:   id,
		Name: user.Name,
	})

	if err != nil {
		return err
	}

	return nil
}
