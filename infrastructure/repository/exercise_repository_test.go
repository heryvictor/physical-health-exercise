//+build !unit

package repository_test

import (
	"context"
	"physical-health/domain"
	"physical-health/domain/motions"
	"physical-health/infrastructure/repository"
	"testing"

	"github.com/stretchr/testify/assert"
)

const databaseTestName = "test_db"

func TestFindOne_Exercise(t *testing.T) {

	mongoInstance, err := repository.NewMongoInstance(
		context.Background(), defaultMongoURL, defaultMongoUsername, defaultMongoPassword)
	assert.NoError(t, err)

	repo := repository.NewExerciseRepository(mongoInstance)
	repo.SetDatabaseName(databaseTestName)
	cleanCollection(t, mongoInstance, databaseTestName, repository.ExerciseCollectionName)

	exercise := createExercise()
	err = repo.Save(exercise)
	assert.NoError(t, err)

	exerciseFound, err := repo.FindById(exercise.ID)
	assert.NoError(t, err)
	assert.NotNil(t, exerciseFound)

	assert.Equal(t, exercise.Name, exerciseFound.Name)
	assert.Equal(t, exercise.Repetitions, exerciseFound.Repetitions)
	assert.Len(t, exercise.Threshold, 2)
}

func createExercise() domain.Exercise {
	movementThreshold := domain.ExerciseThreshold{
		Name:  motions.MovementThresholdName,
		Value: 35,
	}

	highIntensityThreshold := domain.ExerciseThreshold{
		Name:  motions.HighIntensityThresholdName,
		Value: 20,
	}

	exercise := domain.Exercise{
		ID:             "ABD",
		Name:           "ABD - Abduction",
		Repetitions:    5,
		ExpectedMotion: []float64{1.0, 2.0, 3.0, 5.0, 8.0, 13.0, 21.0, 34.0, 21.0, 13.0, 8.0, 5.0, 3.0, 2.0, 1.0},
		Threshold:      []domain.ExerciseThreshold{movementThreshold, highIntensityThreshold},
	}
	return exercise
}
