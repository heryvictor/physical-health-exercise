package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
	"physical-health/domain"
)

const patientCollectionName = "patient"

type (
	PatientRepository struct {
		instance *MongoInstance
	}

	patientData struct {
		ID   string `bson:"_id"`
		Name string `bson:"name"`
	}
)

func NewPatientRepository(instance *MongoInstance) *PatientRepository {

	return &PatientRepository{
		instance: instance,
	}
}

func (r *PatientRepository) FindById(id string) (*domain.Patient, error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(patientCollectionName)

	var patientData patientData
	err = coll.FindOne(ctx, bson.D{{"_id", id}}).Decode(&patientData)

	if err != nil {
		return nil, err
	}

	return &domain.Patient{
		ID:   patientData.ID,
		Name: patientData.Name,
	}, nil
}

func (r *PatientRepository) FindAll() ([]domain.Patient, error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return nil, err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(patientCollectionName)

	cur, err := coll.Find(ctx, bson.D{})

	if err != nil {
		return nil, err
	}

	defer cur.Close(ctx)

	patients := make([]domain.Patient, 0)

	for cur.Next(ctx) {

		var u patientData
		err := cur.Decode(&u)
		if err != nil {
			return nil, err
		}

		patients = append(patients, domain.Patient{
			ID:   u.ID,
			Name: u.Name,
		})
	}

	if err = cur.Err(); err != nil {
		return nil, err
	}

	return patients, nil
}

func (r *PatientRepository) Save(patient domain.Patient) (err error) {

	ctx, cancel := context.WithTimeout(context.Background(), defaultCtxTimeout)
	defer cancel()

	client, err := r.instance.NewClient(ctx)
	if err != nil {
		return err
	}

	defer client.Disconnect(ctx)

	coll := client.Database(databaseName).Collection(patientCollectionName)

	opts := options.Update().SetUpsert(true)

	e := patientData{
		ID:   patient.ID,
		Name: patient.Name,
	}

	_, err = coll.UpdateOne(ctx, bson.D{{"_id", e.ID}}, bson.D{{"$set", e}}, opts)

	if err != nil {
		return err
	}

	return nil
}
