package command

import "flag"

type AppFlags struct {
	KafkaURI        string
	MongodbURI      string
	MongodbUser     string
	MongodbPassword string
}

const (
	defaultKafkaURL      = "localhost:9092"
	defaultMongoURL      = "mongodb://localhost:27017"
	defaultMongoUsername = "mongo"
	defaultMongoPassword = "mongopass"
)

func ParseAppFlags() AppFlags {

	var kafkaURL string
	var mongodbURL string
	var mongodbUsername string
	var mongodbPassword string

	flag.StringVar(&kafkaURL, "kafkaUrl", defaultKafkaURL, "Kafka Brokers")
	flag.StringVar(&mongodbURL, "mongodbUrl", defaultMongoURL, "MongoDb URL")
	flag.StringVar(&mongodbUsername, "mongoUsername", defaultMongoUsername, "MongoDb Username")
	flag.StringVar(&mongodbPassword, "mongoPassword", defaultMongoPassword, "MongoDb Password")

	flag.Parse()

	return AppFlags{
		KafkaURI:        kafkaURL,
		MongodbURI:      mongodbURL,
		MongodbUser:     mongodbUsername,
		MongodbPassword: mongodbPassword,
	}
}
