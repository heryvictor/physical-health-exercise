package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"physical-health/command"
	"physical-health/domain/motions"
	"physical-health/domain/therapists"
	"physical-health/entrypoint/rest"
	"physical-health/infrastructure"
	"physical-health/infrastructure/messaging"
	"physical-health/infrastructure/repository"
	"syscall"
	"time"
)

const port = ":8090"

var version string

func main() {

	ctx, cancel := context.WithCancel(context.Background())

	appFlags := command.ParseAppFlags()
	logger := infrastructure.NewDefaultLogger()

	// Repositories
	mongoInstance, err := repository.NewMongoInstance(ctx,
		appFlags.MongodbURI, appFlags.MongodbUser, appFlags.MongodbPassword)

	if err != nil {
		logger.Error("database connection error: %v", err)
		os.Exit(1)
	}

	movementRepository := repository.NewMovementRepository(mongoInstance)
	therapistRepository := repository.NewTherapistRepository(mongoInstance)
	patientRepository := repository.NewPatientRepository(mongoInstance)
	exerciseRepository := repository.NewExerciseRepository(mongoInstance)

	// Services and Messaging
	therapistService := therapists.NewTherapistService(therapistRepository)
	consumer := messaging.NewKafkaConsumer(appFlags.KafkaURI, logger)
	publisher := messaging.NewKafkaPublisher(appFlags.KafkaURI)

	// HTTP Handlers
	therapistHandler := rest.NewTherapistHandler(therapistService)
	trackMotionHandler := rest.NewTrackMotionHandler(publisher)
	patientHandler := rest.NewPatientHandler(patientRepository)
	exerciseHandler := rest.NewExerciseHandler(movementRepository, exerciseRepository)

	server := rest.New(logger, version).
		SetTherapistHandler(therapistHandler).
		SetPatientHandler(patientHandler).
		SetTrackMotionHandler(trackMotionHandler).
		SetExerciseHandler(exerciseHandler)

	httpServer := &http.Server{
		Addr:    port,
		Handler: server.NewHandler(),
	}

	errChan := startServer(httpServer)

	motionsService := motions.NewTrackMotionsService(consumer, movementRepository, exerciseRepository, logger)
	motionsService.Start(ctx)

	logger.Info("App started. Version: %s", version)

	//  Wait for a Signal to execute the graceful shutdown
	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, syscall.SIGTERM, syscall.SIGINT)

	select {
	case err := <-errChan:
		logger.Error("start server with error: %q", err)

	case sig := <-signalChan:
		logger.Info("signal received %s", sig)

		err := stopServer(httpServer)
		if err != nil && err != http.ErrServerClosed {
			logger.Error("stop server with error %q", err)
			return
		}

		cancel()
		<-ctx.Done()

		logger.Info("Server successfully shutdown")
	}
}

func startServer(httpServer *http.Server) <-chan error {

	errChan := make(chan error)

	go func() {

		if err := httpServer.ListenAndServe(); err != nil {
			errChan <- err
		}

		close(errChan)
	}()

	return errChan
}

func stopServer(httpServer *http.Server) error {

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	return httpServer.Shutdown(ctx)
}
